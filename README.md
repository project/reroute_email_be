# Reroute email Better Experience

The Reroute Email Better Experience module enhances the user
experience related to email rerouting functionality.
This module will provide users with a more intuitive and user-friendly interface for managing email rerouting settings. 

The Reroute Email Better Experience module introduces essential roles
and concepts for effective Data Governance within Drupal. The key roles include:
- Data Owner: Responsible for overall control and management of data.
- Data Steward: Ensures the quality and usability of data.
- Data Custodian: Manages the technical aspects of data storage and access.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/reroute_email_be).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/reroute_email_be).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

- [Reroute Email](https://www.drupal.org/project/reroute_email)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Configuration

- Go to Administration > Extend and enable the module.
- Go to and _/admin/config/development/reroute_email_ and
  _/admin/config/development/reroute_email_be_ .
- Enable Rerouting: Check this box to enable the email rerouting feature.
- Reroute Email Address: Enter the email address where you want to reroute emails.
- TEST EMAIL FORM: Reroute Email also provides a convenient form for testing email
  sending or rerouting. After enabling the module, a test email form is accessible
  under: _/admin/config/development/reroute_email/test_ .
- Skip email rerouting for email addresses: Enter the email addresses which you don't want
  to be rerouted.
- Skip email rerouting for roles: Emails belong to the users with selected roles won't be
  rerouted.


## Maintainers

- Augusto Fagioli - [afagioli](https://www.drupal.org/u/afagioli)
