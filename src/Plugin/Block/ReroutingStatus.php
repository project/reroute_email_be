<?php

namespace Drupal\reroute_email_be\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Rerounting Status' Block.
 *
 * @Block(
 *   id = "rerouting_mail_status_block",
 *   admin_label = @Translation("Rerouting Mail Status"),
 *   category = @Translation("Rerouting Mail Status"),
 * )
 */
class ReroutingStatus extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Instance of the extension path resolver service.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected ExtensionPathResolver $extensionPathResolver;

  /**
   * AdminFeedbackBlock constructor.
   *
   * @param array $configuration
   *   The configuration array.
   * @param string $plugin_id
   *   The plugin ID.
   * @param object $plugin_definition
   *   The plugin definition object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   Instance of the extension path resolver service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, ExtensionPathResolver $extension_path_resolver) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->extensionPathResolver = $extension_path_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('extension.path.resolver'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $rerouteConfig = $this->configFactory->getEditable('reroute_email.settings');

    // Always display rerouting email status.
    if ($rerouteConfig->get("enable") == TRUE) {
      $theme = "reroute_email_be_enabled";
    }
    elseif ($rerouteConfig->get("disabled") == FALSE) {
      $theme = "reroute_email_be_disabled";
    }
    else {
      $theme = "reroute_email_be_missing";
    }

    $path = $this->extensionPathResolver->getPath('module', 'reroute_email_be');

    $renderable = [
      '#theme' => $theme,
      '#path' => $path,
      '#attached' => [
        'library' => [
          'reroute_email_be/main',
        ],
      ],
    ];

    return $renderable;
  }

}
