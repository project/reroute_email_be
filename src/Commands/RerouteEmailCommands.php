<?php

namespace Drupal\reroute_email_be\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * A drush command file.
 *
 * @package Drupal\reroute_email\Commands
 */
class RerouteEmailCommands extends DrushCommands {

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger object factory.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * RerouteEmailCommands constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Config\LoggerChannelFactoryInterface $logger_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct();
    $this->rerouteEmailConfig = $config_factory->getEditable('reroute_email.settings');
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Drush command enabling rerouting.
   *
   * @command reroute_email:enable
   * @aliases rree
   * @usage reroute_email:enable
   */
  public function enableRerouting() {

    $data = [];
    $data['address'] = $this->rerouteEmailConfig->get("address");

    $data['text'] = dt("Email rerouting is now set to 'enabled'. Outgoing email messages
    from this website will be rerouted to !address by the module 'reroute email'", [
      "!address" => $data['address'],
    ]);

    $this->rerouteEmailConfig->set('enable', TRUE)->save();

    // Log event into /admin/reports/dblog.
    $this->loggerFactory->get('reroute_email')->warning($data['text']);

  }

  /**
   * Drush command disabling rerouting.
   *
   * @command reroute_email:disable
   * @aliases rred
   * @usage reroute_email:enable
   */
  public function disableRerouting() {

    $data = [];
    $data['address'] = $this->rerouteEmailConfig->get("address");

    $data['text'] = dt("Email rerouting is now set to 'disabled'. No rerouting in action. Outgoing email messages
    from this website will be delivered to the actual addresses", [
      "!address" => $data['address'],
    ]);

    $this->rerouteEmailConfig->set('enable', FALSE)->save();

    // Log event into /admin/reports/dblog.
    $this->loggerFactory->get('reroute_email')->warning($data['text']);

  }

}
