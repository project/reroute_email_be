<?php

namespace Drupal\reroute_email_be\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\user\RoleStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\reroute_email_be\Constants\RerouteEmailConstants;

/**
 * Implements a settings form for Reroute Email configuration.
 */
class SettingsForm extends ConfigFormBase implements TrustedCallbackInterface {

  /**
   * An editable config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $rerouteConfig;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The role storage.
   *
   * @var \Drupal\user\RoleStorageInterface
   */
  protected $roleStorage;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reroute_email_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['reroute_email.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['textareaRowsValue'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_type.manager')->getStorage('user_role'),
      $container->get('email.validator'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Constructs a new object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\user\RoleStorageInterface $role_storage
   *   The role storage.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              ModuleHandlerInterface $module_handler,
                              RoleStorageInterface $role_storage,
                              EmailValidatorInterface $email_validator,
                              ModuleExtensionList $extension_list_module) {
    parent::__construct($config_factory);
    $this->rerouteConfig = $this->config('reroute_email.settings');
    $this->moduleHandler = $module_handler;
    $this->roleStorage = $role_storage;
    $this->emailValidator = $email_validator;
    $this->moduleExtensionList = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form[RerouteEmailConstants::REROUTE_EMAIL_ENABLE] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable rerouting'),
      '#default_value' => $this->rerouteConfig->get(RerouteEmailConstants::REROUTE_EMAIL_ENABLE),
      '#description' => $this->t('Check this box if you want to enable email rerouting. Uncheck to disable rerouting.'),
      '#config' => [
        'key' => 'reroute_email.settings:' . RerouteEmailConstants::REROUTE_EMAIL_ENABLE,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->rerouteConfig
      ->set(RerouteEmailConstants::REROUTE_EMAIL_ENABLE, $form_state->getValue(RerouteEmailConstants::REROUTE_EMAIL_ENABLE))
      ->set(RerouteEmailConstants::REROUTE_EMAIL_ADDRESS, $form_state->getValue(RerouteEmailConstants::REROUTE_EMAIL_ADDRESS))
      ->set(RerouteEmailConstants::REROUTE_EMAIL_ALLOWLIST, $form_state->getValue(RerouteEmailConstants::REROUTE_EMAIL_ALLOWLIST))
      ->set(RerouteEmailConstants::REROUTE_EMAIL_ROLES, $this->getFilteredRoles($form_state->getValue(RerouteEmailConstants::REROUTE_EMAIL_ROLES)))
      ->set(RerouteEmailConstants::REROUTE_EMAIL_DESCRIPTION, $form_state->getValue(RerouteEmailConstants::REROUTE_EMAIL_DESCRIPTION))
      ->set(RerouteEmailConstants::REROUTE_EMAIL_MESSAGE, $form_state->getValue(RerouteEmailConstants::REROUTE_EMAIL_MESSAGE))
      ->set(RerouteEmailConstants::REROUTE_EMAIL_MAILKEYS, $form_state->getValue(RerouteEmailConstants::REROUTE_EMAIL_MAILKEYS))
      ->set(RerouteEmailConstants::REROUTE_EMAIL_MAILKEYS_SKIP, $form_state->getValue(RerouteEmailConstants::REROUTE_EMAIL_MAILKEYS_SKIP))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
 * Helper function to get filtered roles.
 *
 * @param mixed $roles
 *   The roles value from the form state.
 *
 * @return array
 *   The filtered roles as an array.
 */
  protected function getFilteredRoles($roles): array {
    if (is_array($roles)) {
        return array_values(array_filter($roles));
    }
    return [];
  }

}
